import cv2 as cv
import os
import numpy as np
import tensorflow as tf
from sklearn.model_selection import train_test_split
def normalize_image(image):
    return image / 255.0

Labels = ["su", "makine"]

label_paths = {
    "su": r"C:\Users\teknik\Desktop\dataset\su",
    "makine": r"C:\Users\teknik\Desktop\dataset\makine"
}

output_folder = r'C:\Users\teknik\Desktop\dataset2'

data = []
labels = []

for label in Labels:
    if not os.path.exists(label_paths[label]):
        os.makedirs(label_paths[label])

for label in Labels:
    count = 0
    print("Press 's' to start data collection for " + label)
    user_input = input()
    if user_input != 's':
        print("Wrong Input..........")
        exit()

    image_folder = label_paths[label]

    for filename in os.listdir(image_folder):
        img_path = os.path.join(image_folder, filename)

        frame = cv.imread(img_path, cv.IMREAD_GRAYSCALE)  # Convert to grayscale

        gray_resized = cv.resize(frame, (28, 28))
        gray_normalized = normalize_image(gray_resized)

        data.append(gray_normalized.flatten())
        labels.append(Labels.index(label))  # Index of the current label

print("Data collection and processing completed.")

X_train, X_test, y_train, y_test = train_test_split(np.array(data), np.array(labels), test_size=0.2, random_state=42)

model = tf.keras.Sequential([
    tf.keras.layers.Input(shape=(28 * 28,)),  # Giriş katmanı
    tf.keras.layers.Dense(128, activation='relu'),  # Gizli katman
    tf.keras.layers.Dense(len(Labels), activation='softmax')  # Çıkış katmanı
])

model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])


model.fit(X_train, y_train, epochs=10, validation_data=(X_test, y_test))

test_loss, test_acc = model.evaluate(X_test, y_test)
print(f"\nTest accuracy: {test_acc * 100:.2f}%")

model.save_weights(r'C:\Users\teknik\Desktop\model\weights.h5')

# Yeni bir görüntüyü tahmin etmek için
sample_image = cv.imread(r'C:\Users\teknik\Desktop\ornek\ornek.png', cv.IMREAD_GRAYSCALE)

sample_resized = cv.resize(sample_image, (28, 28))
sample_normalized = normalize_image(sample_resized)
sample_flattened = sample_normalized.flatten()

prediction = model.predict(np.array([sample_flattened]))
predicted_class = Labels[np.argmax(prediction)]

print(f"\nPredicted class: {predicted_class}")
