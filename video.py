import cv2 as cv
import numpy as np
import tensorflow as tf

# Function for image normalization
def normalize_image(image):
    return image / 255.0

# Labels for classes
Labels = ["su", "Makine"]

# Update paths to match your actual file structure
label_paths = {
    "su": r"C:\Users\teknik\Desktop\dataset\su",
    "Makine": r"C:\Users\teknik\Desktop\dataset\makine"
}

# TensorFlow model
model = tf.keras.Sequential([
    tf.keras.layers.Input(shape=(28 * 28,)),  # Input layer
    tf.keras.layers.Dense(128, activation='relu'),  # Hidden layer
    tf.keras.layers.Dense(len(Labels), activation='softmax')  # Output layer
])

# Compile the model
model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

# Load the trained model weights
model.load_weights(r'C:\Users\teknik\Desktop\model\weights.h5')

# Open the camera (0 indicates the default camera)
cap = cv.VideoCapture(0)

while True:
    ret, frame = cap.read()
    gray_frame = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    resized_frame = cv.resize(gray_frame, (28, 28))
    normalized_frame = normalize_image(resized_frame)
    flattened_frame = normalized_frame.flatten()

    # Predict the class of the frame
    prediction = model.predict(np.array([flattened_frame]))
    predicted_class = Labels[np.argmax(prediction)]
    accuracy = np.max(prediction)

    # Display the frame with predicted class and accuracy
    cv.putText(frame, f"Class: {predicted_class}", (10, 30), cv.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv.LINE_AA)
    cv.putText(frame, f"Accuracy: {accuracy * 100:.2f}%", (10, 60), cv.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv.LINE_AA)
    cv.imshow('Object Detection', frame)

    if cv.waitKey(1) & 0xFF == ord('q'):
        break

green = (0, 255, 0)

cap = cv.VideoCapture(0)

while True:
    ret, frame = cap.read()

    rectangle_coordinates = (10, 10, 400, 90)

    cv.rectangle(frame, (rectangle_coordinates[0], rectangle_coordinates[1]),
                 (rectangle_coordinates[0] + rectangle_coordinates[2], rectangle_coordinates[1] + rectangle_coordinates[3]),
                 green, 2)

    text = f"Class: {predicted_class}, Accuracy: {accuracy * 100:.2f}%"
    cv.putText(frame, text, (rectangle_coordinates[0] + 10, rectangle_coordinates[1] + 30),
               cv.FONT_HERSHEY_SIMPLEX, 1, green, 2, cv.LINE_AA)

    cv.imshow('Object Detection', frame)

    if cv.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv.destroyAllWindows()
